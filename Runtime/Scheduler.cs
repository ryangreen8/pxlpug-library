﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VContainer.Unity;
using Random = System.Random;

namespace PxlPug
{
  public class Scheduler : Singleton<Scheduler>, ITickable, IDisposable
  {
    private static List<IScheduledAction> _timeActions = new List<IScheduledAction>();
    private static List<IScheduledAction> _tickActions = new List<IScheduledAction>();
    private static List<IScheduledAction> _pausedActions = new List<IScheduledAction>();
    private static IScheduledAction _curAction;
    private static IScheduledAction _action;
    private static Random _random = new Random();
    private static float _deltaTime;
    private static float[] _frameDeltas = new float[300];
    private static float _deltaAverage;
    private static int _frameIndex;

    protected override void Init()
    {
      Persist = true;
    }

    public void Tick()
    {
      _deltaTime = GameTime.deltaTime;
      UpdateDeltaAverage();
      UpdateTickActions();
      UpdateTimeActions();
    }

    private void UpdateDeltaAverage()
    {
      _frameDeltas[_frameIndex] = _deltaTime;
      _frameIndex = (_frameIndex + 1) % _frameDeltas.Length;
      _deltaAverage = 0;
      for(int i = 0; i < _frameDeltas.Length; i++)
      {
        _deltaAverage += _frameDeltas[i];
      }
      _deltaAverage /= _frameDeltas.Length;
    }

    private void UpdateTickActions()
    {
      for(var i = 0; i < _tickActions.Count; i++)
      {
        _curAction = _tickActions[i];
        _curAction.ticks += 1;
        if(_curAction.ticks == _curAction.targetTicks)
        {
          _curAction.Invoke();
          if(_curAction.repeatCount > 0)
            _curAction.repeatCount--;
          if(_curAction.repeatCount == 0)
          {
            _tickActions.Remove(_curAction);
            i--;
          }
          else
          {
            _curAction.ticks = 0;
          }
        }
      }
    }

    private void UpdateTimeActions()
    {
      for(var i = 0; i < _timeActions.Count; i++)
      {
        _curAction = _timeActions[i];
        _curAction.time += _deltaTime;
        if(_curAction.time >= _curAction.targetTime || CurrentTimeIsCloserThanNextFrame(_curAction.time, _curAction.targetTime))
        {
          _curAction.Invoke();
          if(_curAction.repeatCount > 0)
            _curAction.repeatCount--;
          if(_curAction.repeatCount == 0)
          {
            _timeActions.Remove(_curAction);
            i--;
          }
          else
          {
            _curAction.time = 0;
          }
        }
      }
    }

    private bool CurrentTimeIsCloserThanNextFrame(float currentTime, float targetTime)
    {
      return currentTime + _deltaAverage >= targetTime && targetTime - currentTime < currentTime + _deltaAverage - targetTime;
    }

    public static int DelayInvoke(Action action, float delayInSeconds, string tag = "")
    {
      _action = new ScheduledAction(action);
      return AddTimeAction(delayInSeconds, tag);
    }

    public static int DelayInvoke<TParam1>(Action<TParam1> action, TParam1 param1, float delayInSeconds,
      string tag = "")
    {
      _action = new ScheduledAction<TParam1>(action, param1);
      return AddTimeAction(delayInSeconds, tag);
    }

    public static int DelayInvoke<TParam1, TParam2>(Action<TParam1, TParam2> action, TParam1 param1, TParam2 param2,
      float delayInSeconds, string tag = "")
    {
      _action = new ScheduledAction<TParam1, TParam2>(action, param1, param2);
      return AddTimeAction(delayInSeconds, tag);
    }

    public static int DelayInvoke(Action action, int delayInTicks, string tag = "")
    {
      _action = new ScheduledAction(action);
      return AddTickAction(delayInTicks, tag);
    }

    public static int DelayInvoke<TParam1>(Action<TParam1> action, TParam1 param1, int delayInTicks, string tag = "")
    {
      _action = new ScheduledAction<TParam1>(action, param1);
      return AddTickAction(delayInTicks, tag);
    }

    public static int DelayInvoke<TParam1, TParam2>(Action<TParam1, TParam2> action, TParam1 param1, TParam2 param2,
      int delayInTicks, string tag = "")
    {
      _action = new ScheduledAction<TParam1, TParam2>(action, param1, param2);
      return AddTickAction(delayInTicks, tag);
    }

    public static int DelayInvokeRepeating(Action action, float delayInSeconds, int repeatCount, string tag = "")
    {
      _action = new ScheduledAction(action);
      return AddTimeAction(delayInSeconds, tag, repeatCount);
    }

    public static int DelayInvokeRepeating<TParam1>(Action<TParam1> action, TParam1 param1, float delayInSeconds,
      int repeatCount, string tag = "")
    {
      _action = new ScheduledAction<TParam1>(action, param1);
      return AddTimeAction(delayInSeconds, tag, repeatCount);
    }

    public static int DelayInvokeRepeating<TParam1, TParam2>(Action<TParam1, TParam2> action, TParam1 param1,
      TParam2 param2, float delayInSeconds, int repeatCount, string tag = "")
    {
      _action = new ScheduledAction<TParam1, TParam2>(action, param1, param2);
      return AddTimeAction(delayInSeconds, tag, repeatCount);
    }

    public static int DelayInvokeRepeating(Action action, int delayInTicks, int repeatCount, string tag = "")
    {
      _action = new ScheduledAction(action);
      return AddTickAction(delayInTicks, tag, repeatCount);
    }

    public static int DelayInvokeRepeating<TParam1>(Action<TParam1> action, TParam1 param1, int delayInTicks,
      int repeatCount, string tag = "")
    {
      _action = new ScheduledAction<TParam1>(action, param1);
      return AddTickAction(delayInTicks, tag, repeatCount);
    }

    public static int DelayInvokeRepeating<TParam1, TParam2>(Action<TParam1, TParam2> action, TParam1 param1,
      TParam2 param2, int delayInTicks, int repeatCount, string tag = "")
    {
      _action = new ScheduledAction<TParam1, TParam2>(action, param1, param2);
      return AddTickAction(delayInTicks, tag, repeatCount);
    }

    private static int AddTimeAction(float delayInSeconds, string tag = "", int repeatCount = 1)
    {
      _action.targetTime = delayInSeconds;
      _action.repeatCount = repeatCount;
      _action.id = _random.Next(1000000, 2000000);
      _action.tag = tag;
      _timeActions.Add(_action);
      return _action.id;
    }

    private static int AddTickAction(int delayInTicks, string tag = "", int repeatCount = 1)
    {
      _action.targetTicks = delayInTicks;
      _action.repeatCount = repeatCount;
      _action.id = _random.Next(1000000, 2000000);
      _action.tag = tag;
      _tickActions.Add(_action);
      return _action.id;
    }

    public static void Pause(int id)
    {
      for(var i = 0; i < _timeActions.Count; i++)
      {
        if(_timeActions[i].id != id)
          continue;
        _pausedActions.Add(_timeActions[i]);
        _timeActions.Remove(_timeActions[i]);
        return;
      }

      for(var i = 0; i < _tickActions.Count; i++)
      {
        if(_tickActions[i].id != id)
          continue;
        _pausedActions.Add(_tickActions[i]);
        _tickActions.Remove(_tickActions[i]);
        return;
      }
    }

    public static void Pause(string tag)
    {
      for(var i = _timeActions.Count - 1; i > -1; i--)
      {
        if(_timeActions[i].tag != tag)
          continue;
        _pausedActions.Add(_timeActions[i]);
        _timeActions.Remove(_timeActions[i]);
      }

      for(var i = _tickActions.Count - 1; i > -1; i--)
      {
        if(_tickActions[i].tag != tag)
          continue;
        _pausedActions.Add(_tickActions[i]);
        _tickActions.Remove(_tickActions[i]);
      }
    }

    public static void Resume(int id)
    {
      for(int i = 0; i < _pausedActions.Count; i++)
      {
        if(_pausedActions[i].id != id)
          continue;
        if(_pausedActions[i].targetTicks > 0)
          _tickActions.Add(_pausedActions[i]);
        else
          _timeActions.Add(_pausedActions[i]);
        _pausedActions.Remove(_pausedActions[i]);
        return;
      }
    }

    public static void Resume(string tag)
    {
      for(int i = _pausedActions.Count - 1; i > -1; i--)
      {
        if(_pausedActions[i].tag != tag)
          continue;
        if(_pausedActions[i].targetTicks > 0)
          _tickActions.Add(_pausedActions[i]);
        else
          _timeActions.Add(_pausedActions[i]);
        _pausedActions.Remove(_pausedActions[i]);
      }
    }

    public static void Cancel(int id)
    {
      for(var i = 0; i < _timeActions.Count; i++)
      {
        if(_timeActions[i].id != id)
          continue;
        _timeActions.Remove(_timeActions[i]);
        return;
      }

      for(var i = 0; i < _tickActions.Count; i++)
      {
        if(_tickActions[i].id != id)
          continue;
        _tickActions.Remove(_tickActions[i]);
        return;
      }
    }

    public static void Cancel(Action action)
    {
      for(var i = _timeActions.Count - 1; i >= 0; i--)
      {
        if(_timeActions[i].IsEqual(action))
        {
          _timeActions.Remove(_timeActions[i]);
          return;
        }
      }

      for(var i = _tickActions.Count - 1; i >= 0; i--)
      {
        if(_tickActions[i].IsEqual(action))
        {
          _tickActions.Remove(_tickActions[i]);
          return;
        }
      }
    }

    public static void Cancel<TParam>(Action<TParam> action)
    {
      for(var i = 0; i < _timeActions.Count; i++)
      {
        if(_tickActions[i].IsEqual(action))
        {
          _timeActions.Remove(_timeActions[i]);
          return;
        }
      }

      for(var i = 0; i < _tickActions.Count; i++)
      {
        if(_tickActions[i].IsEqual(action))
        {
          _tickActions.Remove(_tickActions[i]);
          return;
        }
      }
    }

    public static void Cancel<TParam, TParam2>(Action<TParam, TParam2> action)
    {
      for(var i = 0; i < _timeActions.Count; i++)
      {
        if(_tickActions[i].IsEqual(action))
        {
          _timeActions.Remove(_timeActions[i]);
          return;
        }
      }

      for(var i = 0; i < _tickActions.Count; i++)
      {
        if(_tickActions[i].IsEqual(action))
        {
          _tickActions.Remove(_tickActions[i]);
          return;
        }
      }
    }

    public static void Cancel(string tag)
    {
      for(var i = _timeActions.Count - 1; i > -1; i--)
      {
        if(_timeActions[i].tag == tag)
          _timeActions.Remove(_timeActions[i]);
      }

      for(var i = _tickActions.Count - 1; i > -1; i--)
      {
        if(_tickActions[i].tag == tag)
          _tickActions.Remove(_tickActions[i]);
      }
    }

    public static void CancelAll()
    {
      _timeActions.Clear();
      _tickActions.Clear();
    }

    public static Coroutine StartRoutine(IEnumerator coroutine)
    {
      return Instance.StartCoroutine(coroutine);
    }

    public static void StopRoutine(IEnumerator coroutine)
    {
      Instance.StopCoroutine(coroutine);
    }
    
    public static void StopRoutine(Coroutine coroutine)
	{
      Instance.StopCoroutine(coroutine);
	}
    
    public void Dispose()
    {
      _timeActions.Clear();
      _tickActions.Clear();
      _pausedActions.Clear();
      _curAction = null;
      _action = null;
    }
  }
}