using PxlPug.Utils;
using VContainer;
using VContainer.Unity;

namespace PxlPug.Debug
{
  public class TerminalInstaller
  {
    [Inject]
    public static void Install(IContainerBuilder builder, TerminalInput.Settings inputSettings = null, TerminalView.Settings viewSettings = null)
    {
      viewSettings ??= new TerminalView.Settings();
      builder.Register<TerminalView>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf().WithParameter(viewSettings);

      var terminalHistory = JsonFile.Load<TerminalCommandHistory>(TerminalCommandHistory.FILE_PATH) ?? new TerminalCommandHistory();
      builder.RegisterInstance(terminalHistory);

      builder.Register<TerminalController>(Lifetime.Singleton);
      builder.Register<Terminal>(Lifetime.Singleton).AsImplementedInterfaces().AsSelf();

      inputSettings ??= new TerminalInput.Settings();
      builder.RegisterEntryPoint<TerminalInput>().WithParameter(inputSettings);
    }
  }
}