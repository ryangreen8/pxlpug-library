﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PxlPug.Debug
{
  public delegate void CommandHandler(string[] args);

  public struct CommandRegistration
  {
    public string command;
    public string[] commandArray;
    public CommandHandler handler;
    public string help;

    public CommandRegistration(string command, CommandHandler handler, string help)
    {
      this.command = command;
      this.handler = handler;
      this.help = help;

      char[] charsToTrim = {' ', '\''};
      string trimmed = command.Trim(charsToTrim);
      char[] delimiters = {',', ' '};
      commandArray = trimmed.Split(delimiters).ToArray();
    }
  }

  public class TerminalController
  {
    public event Action<string> OnCommand = delegate { };
    public event Action<string> OnOutputWarning = delegate { };
    public event Action<string> OnOutputError = delegate { };
    public event Action<string> OnOutput = delegate { };
    public event Action OnClearOutput = delegate { };
    public event Action OnDeleteCommandHistory = delegate { };

    private Dictionary<string, CommandRegistration> _commands = new Dictionary<string, CommandRegistration>();
    private StringBuilder _builder;
    private char[] _charsToTrim = {' ', '\''};
    private char[] _delimiters = {',', ' '};

    public TerminalController()
    {
      _builder = new StringBuilder();
      RegisterCommand("help", Help, "Prints commands");
      RegisterCommand("clear", ClearOutput, "Clears output");
      RegisterCommand("delete history", DeleteHistory, "Delete terminal command history");
      RegisterCommand("print logs", PrintLogs, "Print Log Messages");
      RegisterCommand("clear logs", ClearLogHistory, "Clear Log History");
    }

    public void RegisterCommand(string command, CommandHandler handler, string help)
    {
      if(!_commands.ContainsKey(command))
        _commands.Add(command, new CommandRegistration(command, handler, help));
    }

    public void UnRegisterCommand(string command)
    {
      _commands.Remove(command);
    }

    public void ParseCommand(string commandString)
    {
      var splitString = SplitString(commandString);
      int bestMatchStrength = 0;
      KeyValuePair<string, CommandRegistration> matchCommand = new KeyValuePair<string, CommandRegistration>();
      foreach(var command in _commands)
      {
        var matchStrength = CalculateMatch(splitString, command.Value);
        if(matchStrength > bestMatchStrength)
        {
          bestMatchStrength = matchStrength;
          matchCommand = command;
        }
      }

      if(bestMatchStrength < 1)
      {
        OutputError($"Unknown command '{commandString}', type 'help' for list.");
        return;
      }

      OnCommand(commandString);
      splitString.RemoveRange(0, bestMatchStrength);
      matchCommand.Value.handler(splitString.ToArray());
    }

    private int CalculateMatch(List<string> commandArr, CommandRegistration registeredCommand)
    {
      if(commandArr.Count < registeredCommand.commandArray.Length)
        return 0;

      var matches = 0;
      for(int i = 0; i < registeredCommand.commandArray.Length; i++)
      {
        if(registeredCommand.commandArray[i].Equals(commandArr[i], StringComparison.OrdinalIgnoreCase))
          matches++;
        else
          return 0;
      }

      return matches;
    }

    private List<string> SplitString(string commandString)
    {
      string trimmed = commandString.Trim(_charsToTrim);
      if(string.IsNullOrEmpty(trimmed))
        return new List<string>();
      return trimmed.Split(_delimiters).ToList();
    }

    private void Help(string[] args)
    {
      _builder.Clear();
      _builder.AppendLine("<b><u>Terminal Commands:</u></b>");
      foreach(var cmd in _commands)
      {
        _builder.AppendLine(
          $"<color=#70a9e9>{cmd.Value.command}</color><pos=20%><color=#A9A9A9>{cmd.Value.help}</color></pos>");
      }

      _builder.AppendLine();
      Output(_builder.ToString());
    }

    private void PrintLogs(string[] args)
    {
      foreach(var entry in Log.history)
      {
        if(entry.type == LogType.Info)
          OnOutput(entry.message);
        else if(entry.type == LogType.Warning)
          OnOutputWarning(entry.message);
        else
          OnOutputError(entry.message);
      }
    }

    private void ClearLogHistory(string[] args)
    {
      Log.ClearHistory();
    }

    private void ClearOutput(string[] args)
    {
      OnClearOutput();
    }

    private void DeleteHistory(string[] args)
    {
      OnDeleteCommandHistory();
    }

    public void OutputError(string line)
    {
      OnOutputError(line);
    }

    public void Output(string line)
    {
      OnOutput(line);
    }

    public void Destroy()
    {
      _commands.Clear();
    }
  }
}