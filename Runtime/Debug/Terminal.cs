﻿using System;
using System.Collections.Generic;
using PxlPug.Utils;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PxlPug.Debug
{
  public class TerminalOpenEvent : GameEvent {}
  public class TerminalCloseEvent : GameEvent {}
  
  [Serializable]
  public class TerminalCommandHistory
  {
    public List<string> commands = new List<string>();
    public static string FILE_PATH = Application.persistentDataPath + "/TerminalCommandHistory.json";
  }
  
  public class Terminal : IInitializable, ILateTickable, IDisposable
  {
    private TerminalView _view;
    private TerminalController _controller;
    private TerminalCommandHistory _commandHistory;
    private TerminalOpenEvent _openEvent;
    private TerminalCloseEvent _closeEvent;
    private int _maxBufferSize = 100;
    private bool _isOpen;
    private int _commandIndex;

    [Inject]
    public Terminal(TerminalView view, TerminalController controller, TerminalCommandHistory commandHistory)
    {
      _view = view;
      _controller = controller;
      _commandHistory = commandHistory;
    }

    public void Initialize()
    {
      CacheComponents();
      AddDelegates();
      _commandIndex = _commandHistory.commands.Count;
    }

    private void CacheComponents()
    {      
      _openEvent = new TerminalOpenEvent();
      _closeEvent = new TerminalCloseEvent();
    }

    private void AddDelegates()
    {
      _view.OnInputSubmit += SubmitInput;
      _controller.OnCommand += AddCommandToHistory;
      _controller.OnOutput += _view.AddOutput;
      _controller.OnOutputWarning += _view.AddWarning;
      _controller.OnOutputError += _view.AddError;
      _controller.OnClearOutput += _view.Clear;
      _controller.OnDeleteCommandHistory += DeleteCommandHistory;
    }

    private void SubmitInput(string txt)
    {
      _controller.ParseCommand(txt);
      _view.ClearInput();
    }
    
    private void AddCommandToHistory(string txt)
    {
      AddToCommandBuffer(txt);
      _view.AddOutputCommand(txt);
      SaveHistory();
    }

    private void AddToCommandBuffer(string txt)
    {
      if(ShouldAddToHistory(txt))
        _commandHistory.commands.Add(txt);
      _commandIndex = _commandHistory.commands.Count;
      if(_commandHistory.commands.Count > _maxBufferSize)
        _commandHistory.commands.RemoveAt(0);
    }
    
    private bool ShouldAddToHistory(string txt)
    {
      var lastIndex = _commandHistory.commands.Count - 1;
      if(lastIndex >= 0)
        return !_commandHistory.commands[lastIndex].Equals(txt);
      return true;
    }
    
    private void DeleteCommandHistory()
    {
      _commandHistory.commands.Clear();
      _commandIndex = 0;
      SaveHistory();
    }
    
    public void LateTick()
    {
      if(_isOpen)
        _view.LateTick();
    }

    public void Open()
    {
      _isOpen = true;
      _view.Show(); 
      Events.Raise(_openEvent);
    }

    public void Close()
    {
      _isOpen = false;
      _view.Hide();
      Events.Raise(_closeEvent);
      SaveHistory();
    }
    
    private void SaveHistory()
    {
      JsonFile.Save(_commandHistory, TerminalCommandHistory.FILE_PATH, true);
    }
    
    public void PreviousCommand()
    {
      _commandIndex--;
      if(_commandIndex < 0)
        _commandIndex = 0;
      SetCommand(_commandIndex);
    }

    public void NextCommand()
    {
      _commandIndex++;
      if(_commandIndex > _commandHistory.commands.Count)
        _commandIndex = _commandHistory.commands.Count;
      SetCommand(_commandIndex);
    }

    private void SetCommand(int historyIndex)
    {
      if(historyIndex < _commandHistory.commands.Count)
        _view.SetCommand(_commandHistory.commands[historyIndex]);
      else
        _view.SetCommand("");
    }

    public void Dispose()
    {
      RemoveDelegates();
      _controller.Destroy();
      _view.Destroy();
      _controller = null;
      _view = null;
      _commandHistory = null;
      _openEvent = null;
      _closeEvent = null;
    }

    private void RemoveDelegates()
    {
      _view.OnInputSubmit -= SubmitInput;
      _controller.OnCommand -= _view.AddOutput;
      _controller.OnOutput -= _view.AddOutput;
      _controller.OnOutputWarning -= _view.AddWarning;
      _controller.OnOutputError -= _view.AddError;
      _controller.OnClearOutput -= _view.Clear;
    }
  }
}