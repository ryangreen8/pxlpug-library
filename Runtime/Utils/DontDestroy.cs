using UnityEngine;

namespace PxlPug.Utils
{
  public class DontDestroy : MonoBehaviour
  {
    private void Awake()
    {
      transform.SetParent(null);
      DontDestroyOnLoad(gameObject);
    }
  }
}