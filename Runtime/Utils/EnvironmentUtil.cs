using System.Linq;

namespace PxlPug.Utils
{
  public static class EnvironmentUtil
  {
    public static string GetCommandLineArg(string argName)
    {
      var args = System.Environment.GetCommandLineArgs();
      for(int i = 0; i < args.Length; i++)
      {
        if(args[i] == argName && args.Length > i + 1)
          return args[i + 1];
      }
      return "";
    }
    
    public static bool HasCommandLineArg(string argName)
    {
      var args = System.Environment.GetCommandLineArgs();
      return args.Contains(argName);
    }
  }
}