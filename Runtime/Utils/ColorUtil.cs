﻿using System.Globalization;
using UnityEngine;

namespace PxlPug.Utils
{
  public class ColorUtil
  {
    public static Color FromHex(string hex)
    {
      hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
      hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
      byte a = 255;//assume fully visible unless specified in hex
      byte r = byte.Parse(hex.Substring(0,2), NumberStyles.HexNumber);
      byte g = byte.Parse(hex.Substring(2,2), NumberStyles.HexNumber);
      byte b = byte.Parse(hex.Substring(4,2), NumberStyles.HexNumber);
      //Only use alpha if the string has enough characters
      if(hex.Length == 8){
        a = byte.Parse(hex.Substring(6,2), NumberStyles.HexNumber);
      }
      return new Color32(r,g,b,a);
    }

    public static string ToHex(Color aColor)
    {
      return string.Format("{0:X}{1:X}{2:X}{3:X}",
        (int)(aColor.r * 255),
        (int)(aColor.g * 255),
        (int)(aColor.b * 255),
        (int)(aColor.a * 255));
    }
  }
}