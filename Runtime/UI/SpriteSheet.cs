using PxlPug.Debug;
using UnityEngine;

namespace PxlPug.UI
{
  public class SpriteSheet
  {
    public string name { get; private set; }
    private Sprite[] _sprites;
    private string[] _spriteNames;

    public SpriteSheet(string sheetName, Object[] sprites)
    {
      name = sheetName;
      _sprites = new Sprite[sprites.Length];
      for (var i = 0; i < sprites.Length; i++)
      {
        _sprites[i] = (Sprite)sprites[i];
      }
      InitNames();
    }

    public SpriteSheet(string sheetName, Sprite[] sprites)
    {
      name = sheetName;
      _sprites = sprites;
      InitNames();
    }

    private void InitNames()
    {
      _spriteNames = new string[_sprites.Length];
      for (var i = 0; i < _sprites.Length; i++)
      {
        _spriteNames[i] = _sprites[i].name;
      }
    }

    public Sprite GetSprite(int index)
    {
      if(index > -1 && index < _sprites.Length)
        return _sprites[index];
 
      // Log.Warning("Sprite Index Is Out of Range");
      return null;
    }

    public Sprite GetSprite(string spriteName)
    {
      for(var i=0; i<_sprites.Length; i++)
      {
        if(_spriteNames[i] == spriteName)
          return _sprites[i];
      }
      // Log.Warning("Sprite with name: " + spriteName + " could not be found");
      return null;
    }

    public int GetCount()
    {
      return _sprites.Length;
    }

    public void Destroy()
    {
      _sprites = null;
      _spriteNames = null;
    }
  }
}