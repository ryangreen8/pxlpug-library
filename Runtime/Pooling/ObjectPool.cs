﻿using System;
using System.Collections.Generic;
using PxlPug.Debug;
using UnityEngine;
using VContainer;
using Random = UnityEngine.Random;

namespace PxlPug.Pooling
{
  public class ObjectPool : IObjectPool
  {
    [Serializable]
    public struct Settings
    {
      public string poolType;
      public int size;
      public bool canExpand;
      public bool forceRecycle;
      public Transform parent;
    }

    private Settings _settings;
    private List<IPoolableFactory> _factories;
    private List<IPoolable> _objects;

    [Inject]
    public ObjectPool(Settings settings, List<IPoolableFactory> factories)
    {
      _settings = settings;
      _factories = factories;
      CreatePool(_settings.size);
    }

    private void CreatePool(int size)
    {
      _objects = new List<IPoolable>();
      if(_settings.size < 1)
      {
        Log.Warning($"Pool {_settings.poolType} is size 0");
        return;
      }
      var objectsPerFactory = size / _factories.Count;
      for(var i=0; i<_factories.Count; i++)
      {
        for(int j = 0; j < objectsPerFactory; j++)
        {
          AddObject(i);
        }
      }
    }

    private void AddObject(int factoryIndex)
    {
      _objects.Add(_factories[factoryIndex].Create());
      _objects[_objects.Count - 1].Recycle();
      if(_settings.parent != null)
        _objects[_objects.Count-1].SetParent(_settings.parent);
    }

    public virtual IPoolable GetObject()
    {
      for(var i=0; i<_objects.Count; i++)
      {
        if(!_objects[i].IsAlive())
          return _objects[i];
      }
      if(_settings.canExpand)
      {
        AddObject(Random.Range(0, _factories.Count-1));
        return _objects[_objects.Count-1];
      }

      if(_settings.forceRecycle)
        return _objects[Random.Range(0, _objects.Count - 1)];
        
      return null;
    }

    public string GetPoolType()
    {
      return _settings.poolType;
    }

    public void Reset()
    {
      for(var i=0; i<_objects.Count; i++)
      {
        _objects[i].Recycle();
      }
    }

    public void Destroy()
    {
      if(_objects == null)
        return;
      for(var i=0; i<_objects.Count; i++)
      {
        _objects[i].Destroy();
      }
      _objects = null;
      _factories = null;
    }
  }
}