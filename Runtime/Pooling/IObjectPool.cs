namespace PxlPug.Pooling
{
  public interface IObjectPool
  {
    string GetPoolType();
    IPoolable GetObject();
    void Reset();
    void Destroy();
  }
}