﻿using System;
using UnityEngine;

namespace PxlPug.Pooling
{
  public interface IPoolable
  {
    event Action<IPoolable> OnRecycle;
    bool IsAlive();
    void Spawn(Vector3 pos);
    void SetParent(Transform parent);
    void Tick();
    void FixedTick();
    void Recycle();
    void Destroy();
  }
}