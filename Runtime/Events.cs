﻿using System.Collections.Generic;

namespace PxlPug
{
  public class GameEvent {}
  public class MainMenuEvent : GameEvent {}
  public class PauseButtonEvent : GameEvent {}
  public class ResumeButtonEvent: GameEvent {}
  public class GamePauseEvent : GameEvent {}
  public class GameResumeEvent : GameEvent {}
  public class GameEnterEvent : GameEvent {}
  
  public static class Events
  {
    public delegate void EventDelegate<T> (T e) where T : GameEvent;
    private delegate void EventDelegate (GameEvent e);

    private static Dictionary<System.Type, EventDelegate> delegates = new Dictionary<System.Type, EventDelegate>();
    private static Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();

    public static void AddListener<T> (EventDelegate<T> del) where T : GameEvent
    {
      if (delegateLookup.ContainsKey(del))
        return;

      EventDelegate internalDelegate = (e) => del((T)e);
      delegateLookup[del] = internalDelegate;

      EventDelegate tempDel;
      if (delegates.TryGetValue(typeof(T), out tempDel))
        delegates[typeof(T)] = tempDel += internalDelegate;
      else
        delegates[typeof(T)] = internalDelegate;
    }

    public static void RemoveListener<T> (EventDelegate<T> del) where T : GameEvent
    {
      EventDelegate internalDelegate;
      if (delegateLookup.TryGetValue(del, out internalDelegate))
      {
        EventDelegate tempDel;
        if (delegates.TryGetValue(typeof(T), out tempDel))
        {
          tempDel -= internalDelegate;
          if (tempDel == null)
            delegates.Remove(typeof(T));
          else
            delegates[typeof(T)] = tempDel;
        }
        delegateLookup.Remove(del);
      }
    }

    public static void Raise (GameEvent e)
    {
      EventDelegate del;
      if (delegates.TryGetValue(e.GetType(), out del))
        del.Invoke(e);
    }
  }
}
