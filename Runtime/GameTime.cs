﻿using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PxlPug
{
  public class GameTime : IInitializable, ITickable
  {
    public static float fixedDeltaTime;
    public static float deltaTime;
    public static float time;
    public static float fixedTime;
    public static float timeScale = 1f;
    public static int frameCount;

    [Inject]
    public GameTime()
    {
    }
    
    public void Initialize()
    {
      Tick();
    }

    public void Tick()
    {
      fixedDeltaTime = Time.fixedDeltaTime * timeScale;
      deltaTime = Time.deltaTime * timeScale;
      time = Time.time * timeScale;
      fixedTime = Time.fixedTime;
      frameCount = Time.frameCount;
    }
  }
}