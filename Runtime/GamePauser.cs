﻿using System;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace PxlPug
{
  public class GamePauser : MonoBehaviour, IInitializable, IDisposable
  {
    [Serializable]
    public class Settings
    {
      public bool pauseOnLostFocus = true;
      public bool zeroTimeScale = true;
      public bool editorPauseEnabled = true;
    }

    private Settings _settings;
    private GamePauseEvent _pauseEvent;
    private GameResumeEvent _resumeEvent;
    private float _timeScale;
    private bool _isPaused;
    private bool _isEnabled;


    [Inject]
    public void Construct(Settings settings)
    {
      _settings = settings;
    }
    
    private void Awake()
    {
      DontDestroyOnLoad(gameObject);
    }

    public void Initialize()
    {
      _pauseEvent = new GamePauseEvent();
      _resumeEvent = new GameResumeEvent();
      DisableScreenSleep();
      AddEventListeners();
    }

    private void AddEventListeners()
    {
      Events.AddListener<PauseButtonEvent>(OnPauseButton);
      Events.AddListener<ResumeButtonEvent>(OnResumeButton);
    }

    private void DisableScreenSleep()
    {
      Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    private void OnPauseButton(PauseButtonEvent e)
    {
      if(_isEnabled && !_isPaused)
        Pause();
    }

    private void OnResumeButton(ResumeButtonEvent e)
    {
      if(_isEnabled && _isPaused)
        Resume();
    }

    public void Pause()
    {
      if(_isPaused)
        return;
      _isPaused = true;
      _timeScale = GameTime.timeScale;
      GameTime.timeScale = 0;
      if(_settings.zeroTimeScale)
        Time.timeScale = 0;
      Events.Raise(_pauseEvent);
      EnableScreenSleep();
    }
    
    private void EnableScreenSleep()
    {
      Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }
    
    public void Resume()
    {
      if(!_isPaused)
        return;
      _isPaused = false;
      GameTime.timeScale = _timeScale;
      if(_settings.zeroTimeScale)
        Time.timeScale = _timeScale;
      Events.Raise(_resumeEvent);
      DisableScreenSleep();
    }

    private void OnApplicationFocus(bool hasFocus)
    {
      if(!hasFocus && _isEnabled && _settings.pauseOnLostFocus)
        Pause();
    }

    private void OnApplicationPause(bool isPaused)
    {
      if(ShouldListenForDeviceEvents())
      {
        if(isPaused)
          Pause();
        else
          Resume();   
      }
    }
    
    private bool ShouldListenForDeviceEvents()
    {
      if(!Application.isEditor)
        return _isEnabled && _settings.pauseOnLostFocus;
      
      return _isEnabled && _settings.editorPauseEnabled;
    }

    public void Enable() => _isEnabled = true;
    public void Disable() => _isEnabled = false;

    public bool IsPaused() => _isPaused;
    public bool IsEnabled() => _isEnabled;

    public void Dispose()
    {
      RemoveEventListeners();
      _settings = null;
      _pauseEvent = null;
      _resumeEvent = null;
    }

    private void RemoveEventListeners()
    {
      Events.RemoveListener<PauseButtonEvent>(OnPauseButton);
      Events.RemoveListener<ResumeButtonEvent>(OnResumeButton);
    }
  }
}