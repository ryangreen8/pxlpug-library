﻿using UnityEngine.UIElements;

namespace PxlPug.Extensions
{
  public static class VisualElementExtensions
  {
    public static void Enable(this Button button) => button.SetEnabled(true);
    public static void Disable(this Button button) => button.SetEnabled(false);
  }
}

