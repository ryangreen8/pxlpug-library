using UnityEngine;

namespace PxlPug.Extensions
{
  public static class AnimatorExtensions
  {
    public static bool IsCurrentState(this Animator animator, int stateHash, int layerIndex = 0)
    {
      return animator.GetCurrentAnimatorStateInfo(layerIndex).fullPathHash == stateHash;
    }

    public static bool IsCurrentState(this Animator animator, string stateName, int layerIndex = 0)
    {
      return animator.GetCurrentAnimatorStateInfo(layerIndex).IsName(stateName);
    }
  }
}
