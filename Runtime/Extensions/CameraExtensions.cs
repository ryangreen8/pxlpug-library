﻿using PxlPug.Debug;
using UnityEngine;

namespace PxlPug.Extensions
{
  public static class OrthographicCameraExtensions
  {
    public static Vector2 GetMinBounds(this Camera camera)
    {
      Vector3 pos = camera.transform.position;
      return new Vector2(pos.x, pos.y) - camera.Extents();
    }

    public static Vector2 GetMaxBounds(this Camera camera)
    {
      Vector3 pos = camera.transform.position;
      return new Vector2(pos.x, pos.y) + camera.Extents();
    }

    public static Vector2 Extents(this Camera camera)
    {
      if(camera.orthographic)
        return new Vector2(camera.orthographicSize * Screen.width / Screen.height, camera.orthographicSize);
      else
      {
        // Log.Error("Camera is not orthographic!", camera);
        return new Vector2();
      }
    }

    public static Bounds OrthographicBounds(this Camera camera)
    {
      float cameraHeight = camera.orthographicSize * 2;
      Bounds bounds = new Bounds(
        camera.transform.position,
        new Vector3(cameraHeight * camera.aspect, cameraHeight, 0));
      return bounds;
    }
  }
}