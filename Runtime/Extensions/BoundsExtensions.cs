using UnityEngine;

namespace PxlPug.Extensions
{
  public static class BoundsExtensions
  {
    public static bool ContainsBounds(this Bounds bounds, Bounds target)
    {
      return (target.max.x < bounds.max.x && target.max.y < bounds.max.y && target.min.x > bounds.min.x && target.min.y > bounds.min.y);
    }
  }
}

