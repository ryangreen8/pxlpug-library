﻿using System.Collections.Generic;
using UnityEngine;

namespace PxlPug.Extensions
{
  public static class TextAssetExtensions
  {
    public static List<string> ToList(this TextAsset ta)
    {
      var list = new List<string>(ta.text.Split('\n'));
      if(list[list.Count - 1] == "")
        list.RemoveAt(list.Count-1);
      return list;
    }
  }
}