﻿using UnityEngine;

namespace PxlPug.Extensions
{
  public static class LayerMaskExtensions
  {
    public static bool Contains(this LayerMask mask, int layer)
    {
      return (mask.value & (1 << layer)) > 0;
    }

    public static bool Contains(this LayerMask mask, GameObject gameobject)
    {
      return (mask.value & (1 << gameobject.layer)) > 0;
    }

    public static LayerMask Invert(this LayerMask mask)
    {
      return ~mask;
    }

    public static LayerMask AddLayer(this LayerMask mask, string layerName)
    {
      return mask |= 1 << LayerMask.NameToLayer(layerName);
    }

    public static LayerMask AddLayers(this LayerMask mask, string[] layerNames)
    {
      for(int i = 0; i < layerNames.Length; i++)
      {
        mask |= 1 << LayerMask.NameToLayer(layerNames[i]);
      }
      return mask;
    }
    
    public static LayerMask RemoveLayer(this LayerMask mask, int layer)
    {
      return mask &= ~(1 << layer);
    }
    
    public static LayerMask RemoveLayer(this LayerMask mask, string layerName)
    {
      return mask &= ~(1<< LayerMask.NameToLayer(layerName));
    }
    
    public static LayerMask RemoveLayers(this LayerMask mask, string[] layerNames)
    {
      for(int i = 0; i < layerNames.Length; i++)
      {
        mask &= ~(1<< LayerMask.NameToLayer(layerNames[i]));
      }
      return mask;
    }
  }
}
