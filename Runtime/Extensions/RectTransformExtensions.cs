﻿using UnityEngine;

namespace PxlPug.Extensions
{
  public static class RectTransformExtensions
  {
    public static void SetSize(this RectTransform rectTransform, Vector2 size)
    {
      var oldSize = rectTransform.rect.size;
      var deltaSize = size - oldSize;
      var pivot = rectTransform.pivot;
      rectTransform.offsetMin -= new Vector2(deltaSize.x * pivot.x,deltaSize.y * pivot.y);
      rectTransform.offsetMax += new Vector2(deltaSize.x * (1f - pivot.x),deltaSize.y * (1f - pivot.y));
    }
 
    public static void SetWidth(this RectTransform rectTransform, float size)
    {
      rectTransform.SetSize(new Vector2(size, rectTransform.rect.size.y));
    }
 
    public static void SetHeight(this RectTransform rectTransform, float size)
    {
      rectTransform.SetSize(new Vector2(rectTransform.rect.size.x, size));
    }
    
    public static Rect ScreenSpaceRect(this RectTransform rectTransform)
    {
      Vector2 size = Vector2.Scale(rectTransform.rect.size, rectTransform.lossyScale);
      return new Rect((Vector2)rectTransform.position - (size * 0.5f), size);
    }
    
    public static void SetAnchorsAndOffsets(this RectTransform rectTransform, Vector2 minAnchor, Vector2 maxAnchor, Vector2 minOffset, Vector2 maxOffset)
    {
      rectTransform.anchorMin = minAnchor;
      rectTransform.anchorMax = maxAnchor;
      rectTransform.offsetMin = minOffset;
      rectTransform.offsetMax = maxOffset;
    }
  }
}