using UnityEngine;
using UnityEngine.UI;

namespace PxlPug.Extensions
{
  public static class ImageExtensions
  {
    private static Color _color;
    
    public static void SetAlpha(this Image image, float alpha)
    {
      _color = image.color;
      _color.a = alpha;
      image.color = _color;
    }
  }
}