using UnityEngine;

namespace PxlPug.Extensions
{
  public static class TransformExtensions
  {
    private static Transform _trans;
    
    public static Transform GetChild(this Transform trans, string childName)
    {
      for(var i = 0; i < trans.childCount; i++)
      {
        _trans = trans.GetChild(i);
        if(_trans.name == childName)
          return _trans;
      }
      return null;
    }

    public static void SetXScale(this Transform trans, float xScale)
    {
      var scale  = trans.localScale;
      scale.x = xScale;
      trans.localScale = scale;
    }
    
    public static void SetYScale(this Transform trans, float yScale)
    {
      var scale  = trans.localScale;
      scale.y = yScale;
      trans.localScale = scale;
    }

    public static void SetZScale(this Transform trans, float zScale)
    {
      var scale  = trans.localScale;
      scale.z = zScale;
      trans.localScale = scale;
    }
  }
}