using UnityEngine;

namespace PxlPug.Extensions
{
  public static class SpriteRendererExtensions
  {
    private static Color _color;
    
    public static void SetAlpha(this SpriteRenderer renderer, float alpha)
    {
      _color = renderer.color;
      _color.a = alpha;
      renderer.color = _color;
    }
  }
}