using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using PxlPug.Debug;

namespace PxlPug.Extensions
{
  public static class GameObjectExtensions
  {
    private static Component _comp;
    private static GameObject _child;
    private static GameObject _ob;
    private static int _obId;
    private static Transform[] _children;

    public static T GetComponentOnSelfOrParent<T>(this GameObject ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp != null)
        return (T)_comp;
      return ob.GetComponentInParent<T>();
    }

    public static T GetComponentOnChild<T>(this GameObject ob, string childName) where T : class
    {
      _child = ob.FindChild(childName);
      return _child == null ? null : _child.GetComponent<T>();
    }

    public static T GetComponentOnChild<T>(this GameObject ob, string childName, bool searchDisabled) where T : Component
    {
      _child = ob.FindChild(childName);
      return _child == null ? null : _child.GetComponentInChildren<T>(searchDisabled);
    }

    // Allows you to search disabled components as well
    public static T GetComponent<T>(this GameObject ob, bool searchDisabled) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length > 0 && comps[0].gameObject == ob)
        return comps[0];
      return null;
    }

    // Allows you to search disabled components as well
    public static T GetComponentInChildren<T>(this GameObject ob, bool searchDisabled) where T: Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length >= 1) return comps[0];
      // Log.Error("Missing " + typeof(T) + "Component", ob);
      return null;
    }

    public static T GetComponentInChildrenOnly<T>(this GameObject ob) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>();
      if(comps.Length > 1)
        return (comps[0].gameObject == ob) ? comps[1] : comps[0];
      return null;
    }

    public static T GetComponentInChildrenOnly<T>(this GameObject ob, bool searchDisabled) where T : Component
    {
      var comps = ob.GetComponentsInChildren<T>(searchDisabled);
      if(comps.Length > 1)
        return (comps[0].gameObject == ob) ? comps[1] : comps[0];
      return null;
    }

    public static T[] GetComponentsInChildrenOnly<T>(this GameObject ob) where T : Component
    {
      var comps = new List<T>(ob.GetComponentsInChildren<T>());
      if(comps.Count > 0)
      {
        if(comps[0].gameObject == ob)
          comps.RemoveAt(0);
        return comps.ToArray();
      }
      return comps.ToArray();
    }

    public static T GetOrAddComponent<T>(this GameObject ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp == null)
        _comp = ob.AddComponent<T>();
      return (T)_comp;
    }

    public static Component GetOrAddComponent(this GameObject ob, string compName)
    {
      _comp = ob.GetComponent(Type.GetType(compName));
      if(_comp == null)
        _comp = ob.AddComponent(Type.GetType(compName));
      return _comp;
    }

    public static void RemoveComponent<T>(this GameObject ob) where T : Component
    {
      _comp = ob.GetComponent<T>();
      if(_comp != null)
        UnityEngine.Object.Destroy(_comp);
    }

    public static void RemoveComponent(this GameObject ob, string compName)
    {
      _comp = ob.GetComponent(Type.GetType(compName));
      if(_comp != null)
        UnityEngine.Object.Destroy(_comp);
    }

    public static GameObject FindChild(this GameObject ob, string name)
    {
      var id = ob.GetInstanceID();
      if(id != _obId)
      {
        _children = ob.GetComponentsInChildren<Transform>(true);
        _obId = id;
      }
      for(var i=0; i<_children.Length; i++)
      {
        if(_children[i].name == name)
          return _children[i].gameObject;
      }
      return null;
    }

    public static GameObject FindChildWithTag(this GameObject ob, string tag)
    {
      Transform[] trans = ob.GetComponentsInChildren<Transform>(true);
      for(var i=0; i<trans.Length; i++)
      {
        if(trans[i].gameObject.tag.Equals(tag))
          return trans[i].gameObject;
      }
      return null;
    }

    public static void SetActiveRecursively(this GameObject ob, bool isActive)
    {
      ob.SetActive(isActive);
      for(var i=0; i<ob.transform.childCount; i++)
      {
        ob.transform.GetChild(i).gameObject.SetActive(isActive);
      }
    }

    public static T FindComponent<T>(this GameObject ob, string name) where T : Component
    {
      _ob  = GameObject.Find(name);
      return (_ob == null) ? null : _ob.GetComponent<T>();
    }

      public static bool IsInLayerMask(this GameObject ob, LayerMask mask)
    {
      return ((mask.value & (1 << ob.layer)) > 0);
    }

    public static bool IsInLayerMask(this GameObject ob, string layerName)
    {
      LayerMask mask = 1 << LayerMask.NameToLayer(layerName);
      return ((mask.value & (1 << ob.layer)) > 0);
    }

    public static T GetInterface<T>(this GameObject ob) where T : class
    {
      return ob.GetComponent(typeof(T)) as T;
    }

    public static void SortChildren(this GameObject gameObject)
    {
      var children = gameObject.GetComponentsInChildren<Transform>(true);
      var sorted = from child in children
        orderby child.gameObject.activeInHierarchy descending, child.name ascending
        where child != gameObject.transform
        select child;
      for(int i = 0; i < sorted.Count(); i++)
      {
        sorted.ElementAt(i).SetSiblingIndex(i);
      }
    }
    
    public static void SetLayerRecursively(this GameObject ob, int layer) 
    {
      ob.layer = layer;
      foreach(Transform child in ob.transform)
        child.gameObject.SetLayerRecursively(layer);
    }
  }
}
