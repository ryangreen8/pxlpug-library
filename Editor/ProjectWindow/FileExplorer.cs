using UnityEngine;

#if UNITY_EDITOR

namespace PxlPug.Editor.ProjectWindow
{
  public class FileExplorer
  {
    public void GuiRender(ProjectWindowStyle style)
    {
      if(GUILayout.Button("Open Project Folder", style.button))
        ProjectUtils.OpenProjectFolder();
     
      if(GUILayout.Button("Open Persistent Data Folder", style.button))
        ProjectUtils.OpenPersistentDataPathFolder();
    }
  }
}

#endif