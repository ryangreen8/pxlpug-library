using System;
using System.Collections.Generic;
using PxlPug.Utils;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace PxlPug.Editor.ProjectWindow
{
  [Serializable]
  public class ProjectWindowState
  {
    public string openScene;
    public bool playFromShell;
  }

  [InitializeOnLoad]
  public class ProjectWindow<TState> : EditorWindow where TState : ProjectWindowState, new()
  {
    private static event Action OnExitingEditModeDelegate = delegate { };
    private static event Action OnEnteredEditModeDelegate = delegate { };
    private static event Action OnExitingPlayModeDelegate = delegate { };
    private static event Action OnEnteredPlayModeDelegate = delegate { };
    
    protected static TState _state;
    protected static List<string> _lockedScenes = new List<string>();
    protected static bool _hasInit;
    protected static ProjectWindowStyle _style;
    protected static SceneExplorer _sceneExplorer;
    protected static FileExplorer _fileExplorer;

    private static string _filePath;
    private static string _fileName = "ProjectWindowState.json";
    private static Vector2 _scrollPos;

    static ProjectWindow()
    {
      EditorApplication.playModeStateChanged += OnPlayModeChanged;
    }

    private void OnEnable()
    {
      if(!_hasInit)
        Init();
    }

    protected virtual void Init()
    {
      _filePath = Application.persistentDataPath + "/" + _fileName;
      _state = (TState) (JsonFile.Load(_filePath, typeof(TState)) ?? new TState());

      _sceneExplorer = new SceneExplorer(_lockedScenes);
      _fileExplorer = new FileExplorer();

      AddDelegates();
      
      _hasInit = true;
    }

    protected virtual void AddDelegates()
    {
      OnExitingEditModeDelegate += OnExitingEditMode;
      OnEnteredEditModeDelegate += OnEnteredEditMode;
      OnExitingPlayModeDelegate += OnExitingPlayMode;
      OnEnteredPlayModeDelegate += OnEnteredPlayMode;
    }

    private void OnDisable()
    {
      EditorApplication.playModeStateChanged -= OnPlayModeChanged;
    }

    private void OnGUI()
    {
      if(!_hasInit)
        Init();

      DefineStyle();
      _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
      RenderSections();
      EditorGUILayout.EndScrollView();
    }
    
    protected virtual void RenderSections()
    {
      RenderSceneSettings();
      RenderPlaySettings();
      RenderFileExplorer();
    }

    protected virtual void DefineStyle()
    {
      _style = new ProjectWindowStyle();
    }

    protected virtual void RenderSceneSettings()
    {
      RenderHeader("Scene Settings");
      _sceneExplorer.GuiRender(_style);
    }

    protected virtual void RenderPlaySettings()
    {
      RenderHeader("Play Settings");
      _state.playFromShell = EditorGUILayout.ToggleLeft("Play From Main Scene", _state.playFromShell);
    }

    protected virtual void RenderFileExplorer()
    {
      RenderHeader("File Explorer");
      _fileExplorer.GuiRender(_style);
    }
    
    protected void RenderHeader(string label)
    {
      GUILayout.Label(label, _style.header);
    }

    private static void OnPlayModeChanged(PlayModeStateChange state)
    {
      switch(state)
      {
        case PlayModeStateChange.EnteredEditMode:
          OnEnteredEditModeDelegate();
          break;
        case PlayModeStateChange.ExitingEditMode:
          OnExitingEditModeDelegate();
          break;
        case PlayModeStateChange.ExitingPlayMode:
          OnExitingPlayModeDelegate();
          break;
        default:
          OnEnteredPlayModeDelegate();
          break;
      }
    }
    
    protected virtual void OnEnteredEditMode()
    {
      if(_state.playFromShell && _sceneExplorer.IsProjectScene(_sceneExplorer.GetOpenSceneName()))
        ReloadPreviousOpenScene();
      
      SetEditModeState();
    }
    
    protected virtual void SetEditModeState()
    {
    }

    protected virtual void OnExitingEditMode()
    {
      JsonFile.Save(_state, _filePath);
      
      _state.openScene = _sceneExplorer.GetOpenSceneName();
      if(!_sceneExplorer.IsProjectScene(_state.openScene))
        return;

      SetPlayState();
      if(_state.playFromShell)
        PlayFromShell();
    }
    
    protected virtual void OnExitingPlayMode()
    {
    }

    protected virtual void OnEnteredPlayMode()
    {
    }
    
    protected virtual void SetPlayState()
    {
    }
    
    protected static void PlayFromShell()
    {
      UnityEditor.SceneManagement.EditorSceneManager.SaveOpenScenes();
      UnityEditor.SceneManagement.EditorSceneManager.OpenScene(_sceneExplorer.GetScenePath(0));
      EditorApplication.isPlaying = true;
    }

    private static void ReloadPreviousOpenScene()
    {
      if(string.IsNullOrEmpty(_state.openScene))
        return;

      var sceneIndex = _sceneExplorer.GetSceneIndex(_state.openScene);
      if(sceneIndex < 0)
        return;

      var scenePath = EditorBuildSettings.scenes[sceneIndex].path;
      UnityEditor.SceneManagement.EditorSceneManager.OpenScene(scenePath);
    }
    
    protected static void DrawUILine(Color color, int thickness = 2, int padding = 10)
    {
      Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
      r.height = thickness;
      r.y += padding / 2;
      r.x -= 2;
      r.width += 6;
      EditorGUI.DrawRect(r, color);
    }
  }
}

#endif