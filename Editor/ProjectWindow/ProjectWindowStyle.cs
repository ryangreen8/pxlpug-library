using PxlPug.Utils;
using UnityEngine;

#if UNITY_EDITOR

namespace PxlPug.Editor.ProjectWindow
{
  public class ProjectWindowStyle
  {
    public GUIStyle header;
    public GUIStyle button;
    public GUIStyle toggle;
    public GUIStyle accordian;
    
    public ProjectWindowStyle()
    {
      header = new GUIStyle();
      header.normal.textColor = Color.white;
      var bgColor = ColorUtil.FromHex("40788d");
      header.normal.background = MakeBackgroundTexture(200, 200, bgColor);
      header.padding = new RectOffset(8, 8, 6, 6);
      header.margin = new RectOffset(0, 0, 16, 4);
      header.fontStyle = FontStyle.Bold;
      header.fontSize = 12;

      button = new GUIStyle(GUI.skin.button);
      button.margin = new RectOffset(4, 4, 6, 6);
      
      toggle = new GUIStyle();
      var highlightedColor = ColorUtil.FromHex("40788d");
      toggle.normal.textColor = highlightedColor;
      toggle.hover.textColor = highlightedColor;
      toggle.active.textColor = highlightedColor;
      toggle.focused.textColor = highlightedColor;
      toggle.fontStyle = FontStyle.BoldAndItalic;

      accordian = new GUIStyle();
      var padding = new RectOffset(20, 20, 2, 10);
      accordian.padding = padding;
      var accordianBgColor = new Color(0.15f, 0.15f, 0.15f);
      accordian.normal.background = MakeBackgroundTexture(200, 200, accordianBgColor);
    }
    
    public Texture2D MakeBackgroundTexture(int width, int height, Color col)
    {
      Color[] pix = new Color[width * height];

      for(int i = 0; i < pix.Length; i++)
        pix[i] = col;

      Texture2D result = new Texture2D(width, height);
      result.SetPixels(pix);
      result.Apply();

      return result;
    }
  }
}

#endif