using UnityEngine;

#if UNITY_EDITOR
using System.IO;
using UnityEditor;
#endif

namespace PxlPug.Editor.Extensions
{
  public static class TextureExtensions
  {
#if UNITY_EDITOR
    public static Texture2D SaveAs(this Texture2D texture, string filePath)
    {
      byte[] bytes = texture.EncodeToPNG();
      FileStream stream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
      BinaryWriter writer = new BinaryWriter(stream);
      for (int i = 0; i < bytes.Length; i++)
        writer.Write(bytes[i]);
      writer.Close();
      stream.Close();
      AssetDatabase.Refresh();
      Texture2D newTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(filePath, typeof(Texture2D));
      if (newTexture == null)
        UnityEngine.Debug.Log("Couldn't Import New Texture: " + filePath);

      return newTexture;
    }
#endif
  }
}