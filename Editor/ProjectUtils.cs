using System.Diagnostics;
using UnityEngine;

namespace PxlPug.Editor
{
  public class ProjectUtils
  {
    public static void OpenProjectFolder()
    {
      OpenFolder($"{Application.dataPath}/../");
    }

    public static void OpenPersistentDataPathFolder()
    {
      OpenFolder($"{Application.persistentDataPath}"); 
    }

    private static void OpenFolder(string folderPath)
    {
      Process proc = new Process();
      proc.StartInfo.FileName = folderPath;
      proc.StartInfo.UseShellExecute = true;
      proc.StartInfo.Verb = "open";
      proc.Start();
    }
  }
}