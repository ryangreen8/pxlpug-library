using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using PxlPug.Debug;
using PxlPug.Utils;
using UnityEditor;

#if UNITY_EDITOR
  
namespace PxlPug.Editor
{
  public class DataSync : AssetPostprocessor
  {
    public struct AssetInfo
    {
      public string fullPath;
      public string folder;
      public string fileName;
      public string extension;
    }

    static void OnPostprocessAllAssets(
      string[] importedAssets,
      string[] deletedAssets,
      string[] movedAssets,
      string[] movedFromAssetPaths)
    {
      foreach(string str in importedAssets)
      {
        var assetPathArr = str.Split('/', '.');
        if(IsAssetAFolder(assetPathArr))
          continue;

        var assetInfo = CreateAssetInfo(str, assetPathArr);
        var linkTypes = FindLinkClasses();
        var syncFiles = BuildSyncFileList(linkTypes);

        for(int i = 0; i < syncFiles.Count; i++)
        {
          if(assetInfo.fileName == syncFiles[i].jsonType.Name || assetInfo.fileName == syncFiles[i].scriptableType.Name)
          {
            UpdateAsset(assetInfo, syncFiles[i]);
            break;
          }
        }
      }
    }

    private static bool IsAssetAFolder(string[] assetPathArr)
    {
      return assetPathArr.Length < 3;
    }

    private static AssetInfo CreateAssetInfo(string str, string[] splitStr)
    {
      return new AssetInfo
      {
        fullPath = str,
        folder = splitStr[splitStr.Length - 3],
        fileName = splitStr[splitStr.Length - 2],
        extension = splitStr[splitStr.Length - 1]
      };
    }

    private static IEnumerable<Type> FindLinkClasses()
    {
      var type = typeof(ISyncLink);
      return AppDomain.CurrentDomain.GetAssemblies()
        .SelectMany(s => s.GetTypes())
        .Where(p => type.IsAssignableFrom(p) && !p.IsInterface);
    }

    private static List<SyncFile> BuildSyncFileList(IEnumerable<Type> linkTypes)
    {
      var files = new List<SyncFile>();
      foreach(var t in linkTypes)
      {
        ConstructorInfo linkConstructor = t.GetConstructor(Type.EmptyTypes);
        object linkClassObject = linkConstructor?.Invoke(new object[] { });
        MethodInfo linkMethod = t.GetMethod("GetFiles");
        if(linkMethod != null) 
          files.AddRange((SyncFile[]) linkMethod.Invoke(linkClassObject, new object[] { }));
      }

      return files;
    }

    private static void UpdateAsset(AssetInfo assetInfo, SyncFile syncFile)
    {
      var ob = AssetDatabaseHelper.FindAsset(syncFile.scriptableType.Name, syncFile.scriptableType);
      if(ob == null)
      {
        Log.Error("Missing Data Sync Object: " + syncFile.scriptableType.Name);
        return;
      }

      FieldInfo dataField = ob.GetType().GetField("data");

      if(assetInfo.extension == "json")
      {
        if(assetInfo.fullPath.Contains("/Resources/"))
          dataField.SetValue(ob, JsonFile.LoadResource(assetInfo.fullPath, syncFile.jsonType));
        else
          dataField.SetValue(ob, JsonFile.Load(assetInfo.fullPath, syncFile.jsonType));
        EditorUtility.SetDirty(ob);
      }
      else
      {
        var path = AssetDatabaseHelper.FindAssetPath(syncFile.jsonType.Name);
        JsonFile.Save(dataField.GetValue(ob), path, true);
      }

      AssetDatabase.SaveAssets();
      AssetDatabase.Refresh();
    }
  }
}

#endif