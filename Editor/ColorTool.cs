using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace PxlPug.Editor
{
  public class ColorTool : EditorWindow
  {
    public string hexString = "FFFFFF";
    public Color color;
    public string colorString;
    
    [MenuItem("PxlPug/Color Tool")]
    public static void ShowWindow()
    {
      GetWindow(typeof(ColorTool), false, "Color Tool");
    }

    private void OnGUI()
    {
      color = EditorGUILayout.ColorField(color);
      hexString = ColorUtility.ToHtmlStringRGB(color);
      hexString = EditorGUILayout.TextField("Hex Color", hexString);
      
      if(ColorUtility.TryParseHtmlString($"#{hexString}", out color))
        colorString = $"{color.r.ToString("F3")}f, {color.g.ToString("F3")}f, {color.b.ToString("F3")}f";
      else
        colorString = "";
      EditorGUILayout.TextField("RGB Values", colorString);
    }
  }
}

#endif